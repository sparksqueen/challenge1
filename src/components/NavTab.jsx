import { Button, ButtonGroup } from "react-bootstrap";
import { Link } from "react-router-dom";
const NavTab = () => {
  return (
    <ButtonGroup
      className="px-5 py-2 d-flex justify-content-center"
      aria-label="Basic example"
    >
      <Button variant="primary" as={Link} to="/create">
        Create Post
      </Button>
      <Button variant="secondary" disabled></Button>
      <Button variant="danger" disabled></Button>
    </ButtonGroup>
  );
};
export default NavTab;
