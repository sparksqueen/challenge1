import { Form, Button } from "react-bootstrap";
import { useState } from "react";
import { NavLink } from "react-router-dom";
import { createBlog } from "../services/ApiCall";

const NewPost = () => {
  const [content, setContent] = useState("");
  const [title, setTitle] = useState("");

  function doPost() {
    createBlog(title, content)
      .then(alert("Datos enviados"))
      .catch((e) => alert(e));
    setTitle("");
    setContent("");
  }
  function handleTitle(e) {
    setTitle(e.target.value);
  }
  function handleContent(e) {
    setContent(e.target.value);
  }
  return (
    <>
      <Button className="m-3" as={NavLink} to="/">
        <i className="fas fa-long-arrow-alt-left"></i>
      </Button>
      <Form className="m-5">
        <Form.Group controlId="exampleForm.ControlInput1">
          <Form.Label>Title</Form.Label>
          <Form.Control type="text" onChange={handleTitle} value={title} />
        </Form.Group>
        <Form.Group controlId="exampleForm.ControlTextarea1">
          <Form.Label>Content</Form.Label>
          <Form.Control
            as="textarea"
            rows={4}
            onChange={handleContent}
            value={content}
          />
        </Form.Group>
        {!title || !content ? (
          <Button className="m-5" variant="secondary" disabled>
            Submit
          </Button>
        ) : (
          <Button className="m-5" onClick={doPost} type="submit" value="Submit">
            Submit
          </Button>
        )}
      </Form>
    </>
  );
};
export default NewPost;
