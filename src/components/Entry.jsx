import { useState, useEffect } from "react";
import { getBlogById } from "../services/ApiCall";
import { Jumbotron, Container, Button } from "react-bootstrap";
import { useParams, NavLink } from "react-router-dom";

const Entry = () => {
  const [entrada, setEntrada] = useState([]);
  const { id } = useParams();
  console.log(id);
  useEffect(() => {
    getBlogById(id).then((result) => setEntrada(result.data));
  }, []);
  return (
    <>
      <Button className="m-3" as={NavLink} to="/">
        <i className="fas fa-long-arrow-alt-left"></i>
      </Button>
      <Jumbotron fluid>
        <Container>
          <h1>{entrada.title}</h1>
          <p>{entrada.body}</p>
        </Container>
      </Jumbotron>
    </>
  );
};
export default Entry;
