import { useEffect, useState } from "react";
import { Card, ListGroup, Col, Row, Button } from "react-bootstrap";
import { deleteBlogById, editBlog, getBlogs } from "../services/ApiCall";
import { NavLink } from "react-router-dom";

const List = () => {
  const [entradas, setEntradas] = useState([]);

  useEffect(() => {
    getBlogs()
      .then((result) => setEntradas(result.data))
      .catch((e) => alert(e));
    console.log(entradas);
  }, []);

  const handleDelete = (id) => {
    deleteBlogById(id)
      .then(setEntradas(entradas.filter((e) => e.id !== id)))
      .then(alert("El post ha sido eliminado"))
      .catch((e) => alert(e));
  };

  const handleEdit = (id) => {
    editBlog(id)
      .then(alert("El post ha sido editado"))
      .catch((e) => alert(e));
  };

  return (
    <Card style={{ backgroundColor: "#c4a8c1" }} className="d-flex p-4">
      <Card.Header style={{ backgroundColor: "#f8e7f6" }}>Entradas</Card.Header>
      <ListGroup>
        {entradas.map((entrada) => {
          return (
            <ListGroup.Item key={entrada.id}>
              <Row>
                <Col className="col-10" as={NavLink} to={`/blog/${entrada.id}`}>
                  {entrada.title}
                </Col>
                <Col className="col-2">
                  <Button onClick={handleEdit}>
                    <i className="fas fa-edit"></i>
                  </Button>
                  <Button
                    variant="danger"
                    onClick={() => handleDelete(entrada.id)}
                  >
                    <i className="fas fa-trash"></i>
                  </Button>
                </Col>
              </Row>
            </ListGroup.Item>
          );
        })}
      </ListGroup>
    </Card>
  );
};
export default List;
