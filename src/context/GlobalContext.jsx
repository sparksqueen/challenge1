import { createContext, useState } from "react";

export const globalContext = createContext();

const GlobalContext = ({ children }) => {
  const [entradas, setEntradas] = useState([]);

  return (
    <globalContext.Provider value={{ entradas, setEntradas }}>
      {children}
    </globalContext.Provider>
  );
};

export default GlobalContext;
