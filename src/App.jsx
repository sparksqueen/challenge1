import "./App.css";
import "bootstrap/dist/css/bootstrap.min.css";
import { useState } from "react";
import NavTab from "./components/NavTab";
import List from "./components/List";
import Entry from "./components/Entry";
import NewPost from "./components/NewPost";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";

function App() {
  return (
    <Router>
      <Switch>
        <Route exact path="/">
          <NavTab />
          <List />
        </Route>
        <Route path="/blog/:id">
          <Entry />
        </Route>
        <Route path="/create">
          <NewPost />
        </Route>
      </Switch>
    </Router>
  );
}

export default App;
