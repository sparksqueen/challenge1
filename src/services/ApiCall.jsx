import axios from "axios";

export const getBlogs = () => {
  return axios.get("https://jsonplaceholder.typicode.com/posts");
};

export const getBlogById = (id) => {
  return axios.get(`https://jsonplaceholder.typicode.com/posts/${id}`);
};

export const createBlog = (params) => {
  return axios.post(`https://jsonplaceholder.typicode.com/posts/`);
};

export const editBlog = (id, params) => {
  return axios.put(`https://jsonplaceholder.typicode.com/posts/${id}`);
};

export const deleteBlogById = (id) => {
  return axios.delete(`https://jsonplaceholder.typicode.com/posts/${id}`);
};
